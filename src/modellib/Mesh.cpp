#include <iostream>
#include <Mesh.hpp>

Mesh::~Mesh()
{
    glDeleteBuffers(1,&vbo);
    glDeleteBuffers(1,&ibo);
    glDeleteVertexArrays(1,&vao);
}

void Mesh::Draw()
{

}

void Mesh::setupMesh()
{
	glGenBuffers(1,&vbo);
	glBindBuffer(GL_ARRAY_BUFFER,vbo);
	glBufferData(GL_ARRAY_BUFFER,vertices.size()*sizeof(Vertex),vertices.data(),GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER,0);

	glGenBuffers(1,&ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,indices.size()*sizeof(unsigned int),indices.data(),GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);

	glGenVertexArrays(1,&vao);
	glBindVertexArray(vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,ibo);
	glEnableVertexAttribArray(0);	//position
	glEnableVertexAttribArray(1);	//normal
	glEnableVertexAttribArray(2);	//texCoords
	glBindBuffer(GL_ARRAY_BUFFER,vbo);
	glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,sizeof(Vertex),(const GLvoid*)(offsetof(Vertex,position)));
	glVertexAttribPointer(1,3,GL_FLOAT,GL_FALSE,sizeof(Vertex),(const GLvoid*)(offsetof(Vertex,normal)));
	glVertexAttribPointer(2,2,GL_FLOAT,GL_FALSE,sizeof(Vertex),(const GLvoid*)(offsetof(Vertex,texCoords)));
	glBindBuffer(GL_ARRAY_BUFFER,0);
	glBindVertexArray(0);
}